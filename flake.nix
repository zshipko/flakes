{
  description = "A collection of flake templates";

  outputs = { self }: {

    templates = {
      basic = {
        path = ./basic;
        description = "A basic flake";
      };
    };

    templates.default = self.templates.basic;
  };
}

