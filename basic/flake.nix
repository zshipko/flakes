{
  description = "A basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let 
          pkgs = import nixpkgs { 
            inherit system; 
          }; 
        in
        with pkgs;
        {
          devShells.default = mkShell {
            buildInput = [raylib];
          };

          defaultPackage = stdenv.mkDerivation {
            name = "hello";
            buildInputs = [clang raylib];
            src = self;
            buildPhase = "cc -o hello hello.c -lraylib";
            installPhase = "mkdir -p $out/bin; install -t $out/bin hello";
          };
        }
      );
}
